package lse;

import java.io.*;
import java.util.*;

public class LSEDriver {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String args[])
	{
		LittleSearchEngine lse = new LittleSearchEngine();
		
		try
		{
			lse.makeIndex("docs.txt", "noisewords.txt");
		} 
		catch (FileNotFoundException e)
		{
		}		
		
		for (String hi : lse.keywordsIndex.keySet())
			System.out.println(hi+" "+ lse.keywordsIndex.get(hi));			

		String word1 = "juice";
		String word2 = "spread";
		System.out.println("Search of " + word1 + " and " + word2 + ": " +lse.top5search(word1, word2));
	}
}//juice [(Cow2.txt,5), (Cow.txt,4), (Cow4.txt,2), (Cow3.txt,1)]
//spread [(Cow4.txt,6), (Cow.txt,5), (Cow3.txt,1), (Cow2.txt,1), (WowCh1.txt,1)]