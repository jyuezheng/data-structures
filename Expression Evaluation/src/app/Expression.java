package app;

import java.io.*;
import java.util.*;
//import java.util.regex.*;

import structures.Stack;

public class Expression {
	// Use for testing: https://dshepsis.github.io/ExpressionGenerator/

	public static String delims = " \t*+-/()&[]";
			
    /**
     * Populates the vars list with simple variables, and arrays lists with arrays
     * in the expression. For every variable (simple or array), a SINGLE instance is created 
     * and stored, even if it appears more than once in the expression.
     * At this time, values for all variables and all array items are set to
     * zero - they will be loaded from a file in the loadVariableValues method.
     * 
     * @param expr The expression
     * @param vars The variables array list - already created by the caller
     * @param arrays The arrays array list - already created by the caller
     */
    public static void 
    makeVariableLists(String expr, ArrayList<Variable> vars, ArrayList<Array> arrays) {
    	String token;
    	String temp = "";
    	for(int i = 0; i < expr.length(); i++)
    	{
    		temp += expr.charAt(i);
    		if(expr.charAt(i) == '[') {temp += '%';} // places % after open bracket
    	}
    	StringTokenizer st = new StringTokenizer(temp, " \t*+-/()&]%"); //if array, has '[' at end of variable name
    	while(st.hasMoreTokens()) {
    		token = st.nextToken();
    		if(token.charAt(token.length() - 1) == '[') {
    			Array newA = new Array(token.substring(0, token.length() - 1));
    			if(arrays.contains(newA)){continue;}
    			arrays.add(new Array(token.substring(0, token.length() - 1)));
    		}
    		else if(Character.isLetter(token.charAt(0)) != true ) {continue;} //if not letter
    		else {
    			Variable newV = new Variable(token);
    			if(vars.contains(newV)){continue;}
    			vars.add(newV);
    		}
    		
    	}
    }
    
    /**
     * Loads values for variables and arrays in the expression
     * 
     * @param sc Scanner for values input
     * @throws IOException If there is a problem with the input 
     * @param vars The variables array list, previously populated by makeVariableLists
     * @param arrays The arrays array list - previously populated by makeVariableLists
     */
    public static void 
    loadVariableValues(Scanner sc, ArrayList<Variable> vars, ArrayList<Array> arrays) 
    throws IOException {
        while (sc.hasNextLine()) {
            StringTokenizer st = new StringTokenizer(sc.nextLine().trim());
            int numTokens = st.countTokens();
            String tok = st.nextToken();
            Variable var = new Variable(tok);
            Array arr = new Array(tok);
            int vari = vars.indexOf(var);
            int arri = arrays.indexOf(arr);
            if (vari == -1 && arri == -1) {
            	continue;
            }
            int num = Integer.parseInt(st.nextToken());
            if (numTokens == 2) { // scalar symbol
                vars.get(vari).value = num;
            } else { // array symbol
            	arr = arrays.get(arri);
            	arr.values = new int[num];
                // following are (index,val) pairs
                while (st.hasMoreTokens()) {
                    tok = st.nextToken();
                    StringTokenizer stt = new StringTokenizer(tok," (,)");
                    int index = Integer.parseInt(stt.nextToken());
                    int val = Integer.parseInt(stt.nextToken());
                    arr.values[index] = val;              
                }
            }
        }
    }
    
    /**
     * Evaluates the expression.
     * 
     * @param vars The variables array list, with values for all variables in the expression
     * @param arrays The arrays array list, with values for all array items
     * @return Result of evaluation
     */
    public static float 
    evaluate(String expr, ArrayList<Variable> vars, ArrayList<Array> arrays) {
    	expr = expr.trim();
    	
    	Stack<String> symbols = new Stack<String>();
    	Stack<Float> values = new Stack<Float>();
    	Stack<String> tempS = new Stack<String>();
    	Stack<Float> tempV = new Stack<Float>();
    	float result = 0, num1 = 0, num2 = 0;
    	String begin = "", end = "", operator = "";
    	
    	System.out.println();
    	System.out.println("--- BEGIN NEW EVALUATE OF {" + expr + "} ---\n");

    	if(indexOpenParen(expr) < 0) { // expression has no parentheses
    		StringTokenizer st = new StringTokenizer(expr, delims, true);
        	String token = "";
        	
        	int negativeCheck = -1;
        	boolean isNegative = false; 
        	
        	while(st.hasMoreTokens()) {
        		token = st.nextToken();
        		if(token.equals(" ")) {continue;}
    			if(token.equals("+") || token.equals("-") || token.equals("/") || token.equals("*")){ // token is an operator
    				if(token.equals("-")) { // check if minus or negative 
    					if(negativeCheck == -1 || negativeCheck > 0) { // is negative and not an operator
    						isNegative = true;
    						negativeCheck = 0;
    						continue;
    					} 
    				} 
    				negativeCheck++;
    
    				if(!tempS.isEmpty() && greaterPrecedence(tempS.peek(), token)) { // if new operation has greater precedence
            			num2 = tempV.pop();
            			num1 = tempV.pop();
            			operator = tempS.pop();
            			tempV.push(operation(operator, num1, num2));
            			System.out.println("EVALUATED: " + num1 +  " " + operator + " " + num2 + " = " + tempV.peek());
            			System.out.println("- Pushed to tempV stack: " + tempV.peek());
    				}
    				tempS.push(token);
    				System.out.println("- Pushed to tempS stack: " + token);
    			}
    			else if (!checkNum(token)) { //token is either variable or array
    				negativeCheck = 0;
    				float value = 0;
    				if(vars.contains(new Variable(token))) { // token is a variable
    					value = vars.get(vars.indexOf(new Variable(token))).value;
    				}
    				else if(arrays.contains(new Array(token))) { // token is an array
    					begin = expr.substring(0, indexOpenBrack(expr) - token.length());
    					end = expr.substring(indexCloseBrack(expr) + 1, expr.length());
    					int valueIndex;
    					valueIndex = (int)evaluate(expr.substring(indexOpenBrack(expr) + 1, indexCloseBrack(expr)), vars, arrays);
    					value = arrays.get(arrays.indexOf(new Array(token))).values[valueIndex];
    					System.out.println();
    					System.out.println(token + "[" + valueIndex + "] = " + value);
    					expr = begin + value + end;
    					return evaluate(expr, vars, arrays);
    				}
    				System.out.println("- Pushed to tempV stack: " + value);
    				tempV.push(value);
    			}
    			else { // token is a number
    				negativeCheck = 0;
    				if(isNegative) {
    					tempV.push(Float.parseFloat(token) * -1);
    					System.out.println("- Pushed to tempV stack: " + "-" + token);
    					isNegative = false;
    				}
    				else {
    					tempV.push(Float.parseFloat(token));
    					System.out.println("- Pushed to tempV stack: " + token);
    				}
    			}
        	}
        	
        	if(!tempS.isEmpty()) { // accounts for last * or /
            	if(tempS.peek().equals("*") || tempS.peek().equals("/")) {
            		num2 = tempV.pop();
            		num1 = tempV.pop();
            		operator = tempS.pop();
            		System.out.print("EVALUATED: " + num1 +  " " + operator + " " + num2 + " = ");
            		tempV.push(operation(operator, num1, num2));
            		System.out.println(tempV.peek());
            	}
        	}
        	
        	// flip stacks to correct order
    		while(!tempV.isEmpty()) {values.push(tempV.pop());}
    		while(!tempS.isEmpty()) {symbols.push(tempS.pop());}
    		
    		System.out.println();
    		
    		// evaluate the two stacks
			num1 = values.pop();
    		while(!symbols.isEmpty()) { 
    			num2 = values.pop();
    			operator = symbols.pop();
    			System.out.print("OPERATION: " + num1 + " " + operator + " " + num2 + " = ");
    			num1 = operation(operator, num1, num2);
    			System.out.println(num1);
    		}
    		return num1;
    	}
    	else { //parentheses present
    		System.out.println("Open paren index: " + indexOpenParen(expr));
        	System.out.println("Close paren index: " + indexCloseParen(expr));
        	
    		if(indexOpenParen(expr) == 0) { begin = ""; }
    		else { begin = expr.substring(0, indexOpenParen(expr)); } 
        	System.out.println("begin: \""+ begin + "\"");
        	
        	if(indexCloseParen(expr) == expr.length() - 1) { end = ""; }
        	else { end = expr.substring(indexCloseParen(expr)+1, expr.length()); }
        	System.out.println("end: \"" + end + "\"");
        	
        	expr = begin + evaluate(expr.substring(indexOpenParen(expr) + 1, indexCloseParen(expr)), vars, arrays) + end;
        	result = evaluate(expr, vars, arrays);
    	}
    	return result;
    }
    /** Returns the index of the first opening parentheses of an expression
     *  Returns -1 if no opening parentheses is found*/
    private static int indexOpenParen(String expr) {
    	for(int i = 0; i < expr.length(); i++) {
    		if(expr.charAt(i) == '(')
    			return i;
    	}
    	return -1;
    }
    
    /** Returns the index of the matching closing parentheses
     *  Returns -1 if no closing parentheses is found*/
    private static int indexCloseParen(String expr) {
    	int count = 0;
    	for(int i = 0; i < expr.length(); i++) {
    		if(expr.charAt(i) == '(') {
    			count++;
    		}
    		else if(expr.charAt(i) == ')') {
    			count--;
    			if(count == 0) {return i;}
    		}
    	}
    	return -1;
    }
    
    /** Returns the index of the first opening bracket of an expression
     *  Returns -1 if not found*/
    private static int indexOpenBrack(String expr) {
    	for(int i = 0; i < expr.length(); i++) {
    		if(expr.charAt(i) == '[')
    			return i;
    	}
    	return -1;
    }
    
    /** Returns the index of the last closing bracket of an expression
     *  Returns -1 if not found*/
    private static int indexCloseBrack(String expr) {
    	int count = 0;
    	for(int i = 0; i < expr.length(); i++) {
    		if(expr.charAt(i) == '[') {
    			count++;
    		}
    		else if(expr.charAt(i) == ']') {
    			count--;
    			if(count == 0) {return i;}
    		}
    	}
    	return -1;
    }
    
    private static float operation(String operator, float num1, float num2) {
    	if(operator.equals("+")) {return num1 + num2;}
    	else if(operator.equals("-")) {return num1 - num2;}
    	else if(operator.equals("*")) {return num1 * num2;}
    	else  {return num1 / num2;} //if(operator.equals("/"))
    }
    
	private static boolean checkNum(String expr){
		try{
			Float.parseFloat(expr);
		}
		catch(Exception ex){
			return false;
		}
		return true;
	}
	
	/** Returns true if symbol1 has greater or same precedence as symbol2*/
	private static boolean greaterPrecedence(String symbol1, String symbol2) {
		if(symbol1.equals("*") || symbol1.equals("/")) {return true;}
		return false;
	}
	
	
}