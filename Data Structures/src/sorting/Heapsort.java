package sorting;

public class Heapsort {

	// prevent objects being created
	private Heapsort() { }
	
	public static <T extends Comparable<T>> 
	void sort(T[] list) {
		// build heap (heapify), linear [O(n)] time 
		for (int k = list.length/2 - 1 ; k >= 0; k--) {
			siftDown(list, k, list.length);
		}
		// sort (nlogn time)
		for (int n = list.length - 1; n > 0; n--) {
			// swap max and last
			T max = list[0];
			list[0] = list[n];
			list[n] = max;
			// sift down from 0, in sub array of length n-1
			siftDown(list, 0, n);
		}
	}
	
	private static <T extends Comparable<T>>
	void siftDown(T[] list, int k, int n) {
		while (2*k+1 < n) {  // not a leaf, there is at least a left child
			int maxIndex = 2*k+1;   // set maxIndex to left child index
			int rightChild = maxIndex + 1;
			if (rightChild < n) { // there is a right child
				if (list[rightChild].compareTo(list[maxIndex]) > 0) { // check if right greater than left
					maxIndex = rightChild; 
				}
			}
			if (list[maxIndex].compareTo(list[k]) > 0) { // max child greater than k, switch
				T temp = list[maxIndex];
				list[maxIndex] = list[k];
				list[k] = temp;
				k = maxIndex;  // for next iteration
			} else {
				break;
			}
		}
	}
}