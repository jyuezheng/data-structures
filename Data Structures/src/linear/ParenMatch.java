package linear;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class ParenMatch {

	public static boolean parenMatch(String expr) {
		Stack<Character> stk = new Stack<>();

		for (int i=0; i < expr.length(); i++) {
			char ch = expr.charAt(i);
			if (ch == '(' || ch == '[') {
				stk.push(ch);  // auto boxes the parameter to Character, then sends to push
				continue;
			}
			if (ch == ')' || ch == ']') {
				try {
					char ch2 = stk.pop();  // might throw a NoSuchElementException (empty stack)
					if (ch == ')' && ch2 != '(') {
						return false;
					}
					if (ch == ']' && ch2 != '[') {
						return false;
					}
				} catch (NoSuchElementException e) {
					return false;
				}
			}
		}
		return stk.isEmpty();   // true if st

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter expression, 'quit' to stop: ");
		String line = sc.nextLine();
		while (!"quit".equals(line)) {
			if (parenMatch(line)) {
				System.out.println("Matched!");
			} else {
				System.out.println("Did not match");
			}
			System.out.print("Enter expression, 'quit' to stop: ");
			line = sc.nextLine();
		}
		sc.close();
	}

}
