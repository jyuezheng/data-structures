package linear;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ALStack<T> {
	
	private ArrayList<T> items;

	public ALStack() {
		items = new ArrayList<T>();
	}

	public void push(T item) {
		items.add(item);
	}

	public T pop() 
	throws NoSuchElementException {
		try {
			return items.remove(items.size()-1);
		} catch (IndexOutOfBoundsException e) {
			throw new NoSuchElementException();
		}
	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

	public int size() {
		return items.size();
	}

	public void clear() {
		items.clear();
	}

}