package search;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class HashMapDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
			throws IOException {
		// TODO Auto-generated method stub
		// set up hash table using constructor with inital N=300, and 
		// load factor threshold = 1.5
		HashMap<String,String> machines = new HashMap<String,String>(300,1.5f); //string, string is key, value

		Scanner sc = new Scanner(new FileReader("ilab_machines.txt"));
		// read lines
		while (sc.hasNextLine()) {
			String[] tokens = sc.nextLine().split(" ");
			
			String key = tokens[0];
			String val = tokens[1];
			
			// put is used for insert as well as update 
			// it will add if key does not exist, and
			// replace old value with new value if key exists
			String oldVal = machines.put(key, val);
			if (oldVal != null) {
				System.out.println("replaced value=" + oldVal + 
								   " with value=" + val + 
								   " for key=" + key);
			} else {
				System.out.println("added mapping " + key + " --> " + val);
			}
			
			// the put method will call the String class's hashCode
			// method (since the key type is String) to get the hash
			// code, then map using mod table size
			// it searches in the mapped location's chain to check
			// if key already exists (using equals). If there is
			// a match, it replaces old
			// value of this key with the value sent in, otherwise
			// adds the key with value
		}
		sc.close();
		
		sc = new Scanner(System.in);
		System.out.print("\nEnter machine you want to look up, or \"quit\": ");
		String machine = sc.nextLine().toLowerCase();
		sc.close();
		while (!"quit".equals(machine)) {
			String room = machines.get(machine);
			
			// the get method will call the String class's hashCode
			// method to obtain the hashCode for the given key
			// (since the key is of type String), then map hash code
			// to table using mod, then search in that chain for
			// the key using equals
			
			if (room != null) {
				System.out.println(machine + " is in " + room);
			} else {
				System.out.println(machine + " is not in the hash table");
			}
			System.out.print("Enter machine you want to look up, or \"quit\": ");
			machine = sc.nextLine().toLowerCase();
		}

		System.out.println("\nHere's a list of all mappings:");
		Set<String> keys = machines.keySet();  // Set is of type String since keys are Strings
		Iterator<String> iterator = keys.iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			System.out.println(key + " --> " + machines.get(key));
			
			
		}
	}
}