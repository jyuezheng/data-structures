package structures;

import java.util.*;

/**
 * This class implements an HTML DOM Tree. Each node of the tree is a TagNode, with fields for
 * tag/text, first child and sibling.
 * 
 */
public class Tree {
	
	/**
	 * Root node
	 */
	TagNode root=null;
	
	/**
	 * Scanner used to read input HTML file when building the tree
	 */
	Scanner sc;
	
	/**
	 * Initializes this tree object with scanner for input HTML file
	 * 
	 * @param sc Scanner for input HTML file
	 */
	public Tree(Scanner sc) {
		this.sc = sc;
		root = null;
	}
	
	/**
	 * Builds the DOM tree from input HTML file, through scanner passed
	 * in to the constructor and stored in the sc field of this object. 
	 * 
	 * The root of the tree that is built is referenced by the root field of this object.
	 */
	public void build() {
		Stack<TagNode> tags= new Stack<TagNode>();
		root = new TagNode("html", null, null);
		tags.push(root); //add html
		sc.nextLine(); //skips over html
		while(sc.hasNextLine()) {
			boolean tag = false;
			String token = sc.nextLine();
			if(token.charAt(0) == '<') { //is tag
				if(token.charAt(1) == '/') {
					tags.pop();
					continue;
				}
				token = token.substring(1, token.length() - 1); //deletes "<>" off tags
				tag = true;
			}
			TagNode temp = new TagNode(token, null, null);
			if(tags.peek().firstChild == null) //temp is first child
				tags.peek().firstChild = temp;
			else { //add to last node
				TagNode ptr = tags.peek().firstChild;
				while(ptr.sibling != null) {
					ptr = ptr.sibling;
				} //ptr is last tagnode
				ptr.sibling = temp;
			}
			if(tag == true) {tags.push(temp);}
		}
	}
	
	/**
	 * Replaces all occurrences of an old tag in the DOM tree with a new tag
	 * 
	 * @param oldTag Old tag
	 * @param newTag Replacement tag
	 */
	public void replaceTag(String oldTag, String newTag) {
		replaceTag(root, oldTag, newTag);
	}
	
	private void replaceTag(TagNode root, String oldTag, String newTag){
		if(root == null) {return;}
		if(root.firstChild != null && root.tag.equals(oldTag)) {
			root.tag = newTag;
		}
		replaceTag(root.firstChild, oldTag, newTag);
		replaceTag(root.sibling, oldTag, newTag);
	}
	
	/**
	 * Boldfaces every column of the given row of the table in the DOM tree. The boldface (b)
	 * tag appears directly under the td tag of every column of this row.
	 * 
	 * @param row Row to bold, first row is numbered 1 (not 0).
	 */
	public void boldRow(int row) {
		TagNode table = findTable(root);
		TagNode tr = table.firstChild;
		int r = 1;
		while(r != row) {
			tr = tr.sibling;
			r++;
		}
		TagNode td = tr.firstChild;
		while(td != null) {
			TagNode bold = new TagNode("b", td.firstChild, null); //check
			td.firstChild = bold;
			td = td.sibling;
		}
	}
	
	/**
	 * Returns table
	 * 
	 * @param root
	 */
	private TagNode findTable(TagNode root){
		if(root == null) {return null;}
		if(root.tag.equals("table")) {return root;}
		TagNode firstChildTable = findTable(root.firstChild);
		if(firstChildTable != null) {return firstChildTable;}
		TagNode siblingTable = findTable(root.sibling);
		if(siblingTable != null) {return siblingTable;}
		return null;
	}
	
	/**
	 * Remove all occurrences of a tag from the DOM tree. If the tag is p, em, or b, all occurrences of the tag
	 * are removed. If the tag is ol or ul, then All occurrences of such a tag are removed from the tree, and, 
	 * in addition, all the li tags immediately under the removed tag are converted to p tags. 
	 * 
	 * @param tag Tag to be removed, can be p, em, b, ol, or ul
	 */
	public void removeTag(String tag) {
		if(tag.equals("p") || tag.equals("em") || tag.equals("b")) {
			remove1(root, tag);
		}
		else if(tag.equals("ol")|| tag.equals("ul")){
			remove2(root, tag);
		}
	}
	
	private void remove1(TagNode root, String tag) {
		if(root == null) {return;}
		if(root.tag.equals(tag) && root.firstChild != null) {
			root.tag = root.firstChild.tag;
			if(root.firstChild.sibling != null) { //find end of siblings and point to root.sibling
				TagNode ptr = root.firstChild;
				while(ptr.sibling != null) { //ptr is last
					ptr = ptr.sibling;
				}
				ptr.sibling = root.sibling;
				root.sibling = root.firstChild.sibling;
			}
			root.firstChild = root.firstChild.firstChild;
		}
		remove1(root.firstChild, tag);
		remove1(root.sibling, tag);
	}
	
	private void remove2(TagNode root, String tag) {
		if(root == null) {return;}
		if(root.tag.equals(tag) && root.firstChild != null){
			root.tag = "p";
			if(root.firstChild.sibling != null) {
				TagNode ptr = root.firstChild;
				while(ptr.sibling != null) { //ends on last <li>
					ptr.tag = "p"; //change <li> to <p>
					ptr = ptr.sibling;
				}
				ptr.tag = "p"; //changes last <li> to <p>
				ptr.sibling = root.sibling;
				root.sibling = root.firstChild.sibling;
				
			}
			root.firstChild = root.firstChild.firstChild;
		}
		remove2(root.firstChild, tag);
		remove2(root.sibling, tag);
	}
	
	/**
	 * Adds a tag around all occurrences of a word in the DOM tree.
	 * 
	 * @param word Word around which tag is to be added
	 * @param tag Tag to be added
	 */
	public void addTag(String word, String tag) {
		addTag(root, word, tag);
	}
	
	private void addTag(TagNode root, String word, String tag) {
		if(root == null) {return;}
		System.out.println(" Root: " + root);
		addTag(root.firstChild, word, tag);
		addTag(root.sibling, word, tag);
		if(root.firstChild == null) { //is plain text
			System.out.println("is plain text");
			while(root.tag.toLowerCase().contains(word.toLowerCase())) { //YES OR NO
				System.out.println("yes contains."); 
				String[] text = root.tag.split(" ");
				boolean found = false;
				String wholeWord = "";
				StringBuilder sb = new StringBuilder();
				int i;
				for(i = 0; i < text.length; i++) {
					System.out.println("Searching for " +  word + " in: " + text[i]);
					if(text[i].toLowerCase().matches(word.toLowerCase() + "[.,!?\\;\\:]?")) { //TOLOWERCASE?
						found = true;
						System.out.println("TAG FOUND.");
						wholeWord = text[i];
						break;
					}
				}
				
				if(!found) {return;}
				
				for(int j = i + 1; j < text.length; j++) {
					sb.append(text[j] + " ");
				}
				String end = sb.toString().trim();
				
				if(i == 0) { //found at beginning
					root.firstChild = new TagNode(wholeWord, null, null);
					root.tag = tag;
					if(!end.equals("") ) {
						TagNode rest = new TagNode(" " + end, null, root.sibling);
						root.sibling = rest;
					}
				}
				else { //middle or end
					//append beginning
					sb = new StringBuilder();
					for(int j = 0; j < i; j++) {sb.append(text[j] + " ");}
					String begin = sb.toString();
					
					//add beginning
					root.tag = begin;
					TagNode newTag = new TagNode(tag, null, root.sibling);
					root.sibling = newTag;
					newTag.firstChild = new TagNode(wholeWord, null, null);
					//add end
					if(!end.equals("") ) { //mid of text
						TagNode rest = new TagNode(" " + end, null, newTag.sibling); //check space!!!!
						//rest.sibling = newTag.sibling;
						newTag.sibling = rest;
					}
				}
				
				
			}
		}
		
	}
	
	/**
	 * Gets the HTML represented by this DOM tree. The returned string includes
	 * new lines, so that when it is printed, it will be identical to the
	 * input file from which the DOM tree was built.
	 * 
	 * @return HTML string, including new lines. 
	 */
	public String getHTML() {
		StringBuilder sb = new StringBuilder();
		getHTML(root, sb);
		return sb.toString();
	}
	
	private void getHTML(TagNode root, StringBuilder sb) {
		for (TagNode ptr=root; ptr != null;ptr=ptr.sibling) {
			if (ptr.firstChild == null) {
				sb.append(ptr.tag);
				sb.append("\n");
			} else {
				sb.append("<");
				sb.append(ptr.tag);
				sb.append(">\n");
				getHTML(ptr.firstChild, sb);
				sb.append("</");
				sb.append(ptr.tag);
				sb.append(">\n");	
			}
		}
	}
	
	/**
	 * Prints the DOM tree. 
	 *
	 */
	public void print() {
		print(root, 1);
	}
	
	private void print(TagNode root, int level) {
		for (TagNode ptr=root; ptr != null;ptr=ptr.sibling) {
			for (int i=0; i < level-1; i++) {
				System.out.print("      ");
			};
			if (root != this.root) {
				System.out.print("|----");
			} else {
				System.out.print("     ");
			}
			System.out.println(ptr.tag);
			if (ptr.firstChild != null) {
				print(ptr.firstChild, level+1);
			}
		}
	}
}
