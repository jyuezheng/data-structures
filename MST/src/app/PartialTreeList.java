package app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import structures.Arc;
import structures.Graph;
import structures.PartialTree;
import structures.Vertex;
import structures.MinHeap;

/**
 * Stores partial trees in a circular linked list
 * 
 */
public class PartialTreeList implements Iterable<PartialTree> {
    
	/**
	 * Inner class - to build the partial tree circular linked list 
	 * 
	 */
	public static class Node {
		/**
		 * Partial tree
		 */
		public PartialTree tree;
		
		/**
		 * Next node in linked list
		 */
		public Node next;
		
		/**
		 * Initializes this node by setting the tree part to the given tree,
		 * and setting next part to null
		 * 
		 * @param tree Partial tree
		 */
		public Node(PartialTree tree) {
			this.tree = tree;
			next = null;
		}
	}

	/**
	 * Pointer to last node of the circular linked list
	 */
	private Node rear;
	
	/**
	 * Number of nodes in the CLL
	 */
	private int size;
	
	/**
	 * Initializes this list to empty
	 */
    public PartialTreeList() {
    	rear = null;
    	size = 0;
    }

    /**
     * Adds a new tree to the end of the list
     * 
     * @param tree Tree to be added to the end of the list
     */
    public void append(PartialTree tree) {
    	Node ptr = new Node(tree);
    	if (rear == null) {
    		ptr.next = ptr;
    	} else {
    		ptr.next = rear.next;
    		rear.next = ptr;
    	}
    	rear = ptr;
    	size++;
    }

    /**
	 * Initializes the algorithm by building single-vertex partial trees
	 * 
	 * @param graph Graph for which the MST is to be found
	 * @return The initial partial tree list
	 */
	public static PartialTreeList initialize(Graph graph) {	
		PartialTreeList ptl = new PartialTreeList();
		for(int i = 0; i < graph.vertices.length; i++) {
			//1. create a partial tree pt containing only v
			PartialTree pt = new PartialTree(graph.vertices[i]);
			
			//2. mark v as belonging to pt
			Vertex v = graph.vertices[i];
			
			Vertex.Neighbor neighbor = v.neighbors;
			
			//3. create a priority queue (heap) and associate it with pt
			MinHeap<Arc> pq = pt.getArcs();
			
			//4. insert all of the arcs connected to v into priority queue
			//lower the weight, higher the priority
			while(neighbor != null) {
				Arc arc = new Arc(v, neighbor.vertex, neighbor.weight);
				pq.insert(arc);
				neighbor = neighbor.next;
			}
			
			//5. add partial tree (pt) to list (ptl)
			ptl.append(pt);
			//test
			//System.out.println(pt.toString());
		}
		return ptl;
	}
	
	/**
	 * Executes the algorithm on a graph, starting with the initial partial tree list
	 * for that graph
	 * 
	 * @param ptlist Initial partial tree list
	 * @return Array list of all arcs that are in the MST - sequence of arcs is irrelevant
	 */
	public static ArrayList<Arc> execute(PartialTreeList ptlist) {
		ArrayList<Arc> arcs = new ArrayList<Arc>();
		
		while(ptlist.size() > 1) {
			//1. remove first partial tree ptx from list
			//   pqx = priority queue of ptx
			PartialTree ptx = ptlist.remove();
			MinHeap<Arc> pqx = ptx.getArcs();
			
			//2. remove highest-priority arc (hpArc) from pqx
			//	 v1, v2 two vertices connected by hpArc, v1 belongs to ptx
			Arc hpArc = pqx.deleteMin();
			//Vertex v1 = hpArc.getv1();
			Vertex v2 = hpArc.getv2();
			
			//3. if v2 also belongs to ptx, pick next highest priority arc
			while(v2.getRoot() == ptx.getRoot()) {
				hpArc = pqx.deleteMin();
				v2 = hpArc.getv2();
			}
			
			//4. report arc; arc is a component of the MST
			arcs.add(hpArc);
			
			//5. find the partial tree pty to which v2 belongs
			//   remove PTY from the ptlist
			//   let pqy be pty's priority queue.
			PartialTree pty = ptlist.removeTreeContaining(v2);
			
			//6. combine ptx and pty, includes merging pqx and pqy into single priority queue
			//	 append the resulting tree to the end of ptlist
			ptx.merge(pty);
			ptlist.append(ptx);
		}
		return arcs;
	}
	
    /**
     * Removes the tree that is at the front of the list.
     * 
     * @return The tree that is removed from the front
     * @throws NoSuchElementException If the list is empty
     */
    public PartialTree remove() 
    throws NoSuchElementException {
    			
    	if (rear == null) {
    		throw new NoSuchElementException("list is empty");
    	}
    	PartialTree ret = rear.next.tree;
    	if (rear.next == rear) {
    		rear = null;
    	} else {
    		rear.next = rear.next.next;
    	}
    	size--;
    	return ret;
    		
    }

    /**
     * Removes the tree in this list that contains a given vertex.
     * 
     * @param vertex Vertex whose tree is to be removed
     * @return The tree that is removed
     * @throws NoSuchElementException If there is no matching tree
     */
    public PartialTree removeTreeContaining(Vertex vertex) 
    throws NoSuchElementException {
    		if(rear == null) {
    			throw new NoSuchElementException();
    		}
    		
    		Node ptr = rear;
    		PartialTree tree = null;
    		
    		if(size == 1) {
    			if(vertex.getRoot().equals(ptr.tree.getRoot())) {
    				size--;
    				rear = null;
    				return ptr.tree;
    			}
    			throw new NoSuchElementException();
    		}
    		
    		Node prev = rear;
    		ptr = rear.next;
    		do {
    			if(vertex.getRoot().equals(ptr.tree.getRoot())) {
    				if(vertex.getRoot().equals(rear.tree.getRoot())) {
    					tree = rear.tree;
    					prev.next = rear.next;
    					rear = prev;
    					size--;
    					return tree;
    				}
    				tree = ptr.tree;
    				prev.next = ptr.next;
    				ptr = ptr.next;
    				size--;
    				return tree;
    			}
    			prev = ptr;
    			ptr = ptr.next;
    		} while(ptr != rear.next);
    		
    		throw new NoSuchElementException();
    		
     }
    
    /**
     * Gives the number of trees in this list
     * 
     * @return Number of trees
     */
    public int size() {
    	return size;
    }
    
    /**
     * Returns an Iterator that can be used to step through the trees in this list.
     * The iterator does NOT support remove.
     * 
     * @return Iterator for this list
     */
    public Iterator<PartialTree> iterator() {
    	return new PartialTreeListIterator(this);
    }
    
    private class PartialTreeListIterator implements Iterator<PartialTree> {
    	
    	private PartialTreeList.Node ptr;
    	private int rest;
    	
    	public PartialTreeListIterator(PartialTreeList target) {
    		rest = target.size;
    		ptr = rest > 0 ? target.rear.next : null;
    	}
    	
    	public PartialTree next() 
    	throws NoSuchElementException {
    		if (rest <= 0) {
    			throw new NoSuchElementException();
    		}
    		PartialTree ret = ptr.tree;
    		ptr = ptr.next;
    		rest--;
    		return ret;
    	}
    	
    	public boolean hasNext() {
    		return rest != 0;
    	}
    	
    	public void remove() 
    	throws UnsupportedOperationException {
    		throw new UnsupportedOperationException();
    	}
    	
    }
}


