package app;

//import structures.Arc;
import structures.Graph;
import structures.PartialTree;

import java.util.Iterator;


import java.io.IOException;
//import java.util.ArrayList;


public class PartialTreeListDriver {
	public static void main(String[] args) {
		Graph graph = null;
		try {
		graph = new Graph("graph3.txt");
		}
		catch (IOException e) {
            e.printStackTrace();
        }
		graph.print();
		PartialTreeList ptl = PartialTreeList.initialize(graph);
		Iterator<PartialTree> iter = ptl.iterator();
        while(iter.hasNext()) {
            System.out.println(iter.next());
        }

        System.out.println("MST: " + PartialTreeList.execute(ptl) + "\n\n\n");
        /*iter = ptl.iterator();
        while(iter.hasNext()) {
            System.out.println(iter.next());
        }*/
		
	}
	
}
